/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.interfaceproject;

/**
 *
 * @author sippu
 */
public class TestFlyable {
    public static void main(String[] args) {
        Bat bat =new Bat();
        Plane plane = new Plane("Engine number I");
        bat.fly();
        plane.fly();
        Dog dog = new Dog();
        Flyable[] flyable ={bat,plane};
        for(Flyable f:flyable){
            if(f instanceof Plane){
                Plane p = (Plane)f;
                p.startEngine();
                p.run();
            }
            f.fly();
        }
        
        Runable[] runable = {dog,plane};
        for(Runable r: runable){
            r.run();
        }
    }
}
